import dgram from "dgram";
import { io } from "./server";
import events from "./lib/socket-io-events";

import messageParser from "./messageParser";

const PORT = process.env.UDP_PORT || 33333;
const HOST = process.env.UDP_HOST || "127.0.0.1";
const udpserver = dgram.createSocket("udp4");

//this could be used for having control over the frequency we sent data to visualisation
//const signalInterval = process.env.SIGNAL_INTERVAL || 125;

const sendToClient = result => {
  // console.log("emitting to client----", result);
  io.sockets.emit(events.MESSAGE_RECEIVED, result);
};

let currentResult = {};

udpserver.on("listening", () => {
  const address = udpserver.address();
  console.log(
`        Configure your UDP signal to be sent on
            ${address.address}:${address.port}         
          
/////////////////////////////////////////////////////////`
  );
});

udpserver.on("message", (message, remote) => {
  const result = messageParser(message);
  if (result) {
    //this could be used for having control over the frequency we sent data to visualisation
    // currentResult = result;
    sendToClient(result); //comment this out if you use the above method
  }
});

// this could be used for having control over the frequency we sent data to visualisation
// setInterval(()=>{sendToClient(currentResult)},signalInterval)

udpserver.bind(PORT, HOST);

export default udpserver;
