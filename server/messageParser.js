const transformation = value => value*2-1
const messageParser = (message)=>{

    const bufLen = message.toString().length
    if (bufLen == 16) {
            // for (const pair of message.entries()) {
            //     console.log(pair);
            // }
        let header = message.toString("utf8",0,4)

        const valence= transformation(message.readFloatLE(4))
        const arousal = transformation(message.readFloatLE(8))
        const attention = message.readFloatLE(12)
        return {
            header,
            valence,
            arousal,
            attention
        }
    } else return false

}

export default messageParser
