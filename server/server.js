import express from "express";
import path from "path";
import http from "http";
import cookieParser from "cookie-parser";
import bodyParser from "body-parser";
import helmet from "helmet";
import compression from "compression";
import SocketIO from "socket.io";

const PORT = process.env.EXPRESS_PORT || 3001;
const oneWeek = 7 * 24 * 60 * 60 * 1000;
const expressApp = express();
const server = http.Server(expressApp);
const io = new SocketIO(server);

io.origins("*:*");

expressApp.use(compression({}));
expressApp.use(cookieParser());
expressApp.use(bodyParser.json());
expressApp.use(helmet());

if (process.env.NODE_ENV === "production") {
  expressApp.use(
    express.static(path.resolve(__dirname, "../client/build"), {
      maxage: oneWeek
    })
  );
}

io.on("connection", socket => {
  socket.on("disconnect", () => {
    console.log("socket disconnected");
  });
});

server.listen(PORT, err => {
  if (!err && process.env.NODE_ENV === "production") console.log(
`/////////////////////////////////////////////////////////
                                                     
            Your visualisation is running at             
               http://127.0.0.1:${PORT}                 
            Open a browser and navigate to it.
`);
});

//exports

export { io, expressApp };
