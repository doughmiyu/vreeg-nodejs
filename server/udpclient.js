const dotenv = require("dotenv").config();
const dgram = require("dgram");
const PORT = parseInt(process.env.UDP_PORT) || 33333;
const HOST = process.env.UDP_HOST || "127.0.0.1";
const FREQUENCY = process.env.UDP_MOCK_FREQUENCY || 125;

const sendMessage = () => {
  const message = Buffer.alloc(16);
  message.write("1234");
  message.writeFloatLE(Math.random(), 4);
  message.writeFloatLE(Math.random(), 8);
  message.writeFloatLE(Math.random(), 12);

  const client = dgram.createSocket("udp4");
  client.send(message, 0, message.length, PORT, HOST, function(err, bytes) {
    if (err) throw err;
    //console.log(`UDP message sent to ` + HOST + ":" + PORT, bytes);
    client.close();
  });
};

console.log(`You should see messages sent every ${FREQUENCY} ms`);
setInterval(sendMessage, FREQUENCY);
