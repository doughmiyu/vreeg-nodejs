# EEG emotion visualisation

## How to run
- clone the app to a local folder
- go to that local folder and run `npm run build-and-start`
- when the message is printed you open `http://127.0.0.1:3001` in any browser that you like on the machine
- the device should be configured to send UDP message to `127.0.0.1:33333`

## Dev
- you have to start each component in its separate terminal
- `cd ./server && npm run dev` for server start
- `cd ./server && npm run signal-generator` for mock signal generator
- `cd ./client && npm start` for react client

Any changes you do on the files the client server and signal generator will be reloaded
