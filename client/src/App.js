import Chart from "chart.js";
import "chartjs-plugin-streaming";
import "chartjs-plugin-style";
import _ from "lodash";
import React, { Component } from "react";
import io from "socket.io-client";
import events from "./lib/socket-io-events";
import explanations from "./lib/explanations";
import "./App.scss";
import logoSrc from "./assets/imec_white.png";

let socket;

const defaultLineDataSet = {
  fill: false,
  pointRadius: 0,
  borderWidth: 3,
  borderColor: "rgb(255,255,255)",
  lineTension: 0.3,
  shadowOffsetX: 1,
  shadowOffsetY: 2,
  shadowBlur: 15,
  shadowColor: "rgba(0, 0, 0, 0.5)",
  cubicInterpolationMode: "monotone"
};

const defaultLineScales = {
  xAxes: [
    {
      display: false,
      gridLines: {
        zeroLineColor: "rgba(0,0,0,0)",
        color: "rgba(0,0,0,0)"
      },
      type: "realtime", // x axis will auto-scroll from right to left
      realtime: {
        // per-axis options
        duration: 60 * 1000,
        delay: 125,
        pause: false, // chart is not paused
        ttl: undefined
      }
    }
  ],
  yAxes: [
    {
      // display: false,
      ticks: {
        min: -1,
        max: 1,
        stepSize: 1,
        fontFamily:
          "'Gill Sans MT-Regular', 'Gill Sans', 'Gill Sans MT', sans-serif",
        fontSize: 18,
        fontColor: "rgb(205,206,222)"
      },
      gridLines: {
        zeroLineColor: "rgba(0,0,0,0)",
        color: "rgba(0,0,0,0)"
      }
    }
  ]
};

const defaultLinePlugins = {
  streaming: {
    // per-chart option
    frameRate: 30 // chart is drawn 30 times every second
  }
};

const defaultLineOptions = {
  legend: {
    display: false
  },
  maintainAspectRatio: false,
  scales: {
    ...defaultLineScales
  },
  title: {
    display: false
  },
  plugins: {
    ...defaultLinePlugins
  }
};

class App extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      topChart: {
        sizesFetched: false,
        width: 0,
        height: 0
      },
      realtimeData: {
        valence: 0,
        arousal: 0,
        attention: 0
      },
      chartData: {
        header: [],
        valence: [],
        happy: [],
        attention: [],
        thinking: []
      }
    };
    this.setupSocket = this.setupSocket.bind(this);
    this.configHappiness = this.configHappiness.bind(this);
  }

  componentWillMount() {}

  componentDidMount() {
    this.setupCharts();
    setTimeout(this.setupSocket, 1000);
  }

  setupSocket() {
    socket = io("http://localhost:3001");

    socket.on("disconnect", () => {
      console.log("Connection lost...");
    });

    socket.on("connect_error", () => {
      console.log("Waiting on server connection...");
    });

    socket.on(events.MESSAGE_RECEIVED, evtData => {
      // console.log("data...", evtData);

      const { valence, arousal, attention } = evtData;
      this.setState({
        realtimeData: {
          valence,
          arousal,
          attention
        }
      });

      let firstLinearChart = window.valence;
      let secondLinearChart = window.attention;
      let timeStamp = new Date().getTime();
      window.happiness.data.datasets[0].data = [
        {
          x: evtData.valence,
          y: evtData.arousal
        }
      ];
      window.happiness.update({});
      firstLinearChart.data.datasets[1].data.push({
        x: timeStamp,
        y: evtData.valence
      });
      firstLinearChart.data.datasets[0].data.push({
        x: timeStamp,
        y: evtData.arousal
      });
      firstLinearChart.update({
        preservation: true
      });
      secondLinearChart.data.datasets[0].data.push({
        x: timeStamp,
        y: evtData.attention
      });
      secondLinearChart.update({
        preservation: true
      });
    });
  }

  componentWillUnmount() {
    socket.disconnect();
  }
  configHappiness() {
    return {
      type: "scatter", // 'line', 'bar', 'bubble' and 'scatter' types are supported
      data: {
        datasets: [
          {
            data: [
              {
                x: 0,
                y: 0
              }
            ],
            label: "happy",
            fill: true,
            backgroundColor: "rgba(75,192,192,0.4)",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointRadius: 10,
            pointHitRadius: 0,
            shadowOffsetX: 1,
            shadowOffsetY: 2,
            shadowBlur: 15,
            shadowColor: "rgba(0, 0, 0, 0.5)"
          }
        ]
      },
      options: {
        events:[],
        maintainAspectRatio: false,
        legend: {
          display: false
        },
        scales: {
          xAxes: [
            {
              // display: false,
              id: "x-axis-1",
              ticks: {
                // display: false,
                //   padding:-100,
                min: -1,
                max: 1,
                stepSize: 0.5,
                callback: (value, index) => {
                  if (value === 0) return "";
                  else return value;
                },
                fontFamily:
                  "'Gill Sans MT-Regular', 'Gill Sans', 'Gill Sans MT', sans-serif",
                fontSize: 18,
                fontColor: "rgb(205,206,222)"
              },
              gridLines: {
                drawTicks: true,
                zeroLineColor: "rgba(255,255,255,1)",
                color: "rgba(0,0,0,0)"
              }
            }
          ],
          yAxes: [
            {
              id: "y-axis-1",
              // display: false,
              ticks: {
                // display: false,
                min: -1,
                max: 1,
                stepSize: 0.5,
                callback: (value, index) => {
                  if (value === 0) return "";
                  else return value;
                },
                fontFamily:
                  "'Gill Sans MT-Regular', 'Gill Sans', 'Gill Sans MT', sans-serif",
                fontSize: 18,
                fontColor: "rgba(205,206,222)"
              },
              gridLines: {
                drawTicks: true,
                zeroLineColor: "rgba(255,255,255,1)",
                color: "rgba(0,0,0,0)"
              }
            }
          ]
        },
        title: {
          display: false,
          text: "Happiness"
        }
      },
      plugins: [
        {
          beforeDraw: function(c) {
            const chartHeight = c.chart.height;
            const chartWidth = c.chart.width;
            const fontSize = (chartHeight * 4.5) / 100;
            const titleFontSize = (chartHeight * 5) / 100;
            const xAxis1 = c.scales["x-axis-1"];
            const yAxis1 = c.scales["y-axis-1"];
            // console.log(chartHeight)
            // console.log("scales", c.scales);
            //   console.log(c)
            //drawing titles
            const ctx = c.ctx;
            ctx.fillStyle = "rgb(255,255,255)";
            ctx.font = `${titleFontSize}px 'Gill Sans MT-Regular', 'Gill Sans', 'Gill Sans MT', sans-serif`;
            ctx.fillText(
              "AROUSAL",
              chartWidth / 2 - 4.25 * titleFontSize,
              titleFontSize*1.1
            );
            ctx.fillText(
              "VALENCE",
              chartWidth - 4.5 * titleFontSize,
              chartHeight / 2 - titleFontSize
            );
            xAxis1.options.ticks.minor.fontSize = fontSize;
            xAxis1.options.ticks.minor.padding =
              -chartHeight / 2 + fontSize + 3;
            yAxis1.options.ticks.minor.fontSize = fontSize;
            yAxis1.options.ticks.minor.padding = -chartWidth / 2 - fontSize*1.29;
            yAxis1.options.ticks.minor.labelOffset = (chartHeight * 2.5) / 100;

          }
        }
      ]
    };
  }

  setupCharts() {
    const configHappiness = this.configHappiness();
    const beforeDrawForLines = c => {
      // console.log("scales", c.scales);
      const chartHeight = c.chart.height;
      // const chartWidth = c.chart.width;
      const fontSize = (chartHeight * 10) / 100;
      c.scales["y-axis-0"].options.ticks.minor.fontSize = fontSize;
    };
    const configValence = {
      type: "line",
      data: {
        datasets: [
          {
            ...defaultLineDataSet,
            borderColor: "rgb(82,189,194)",
            data: []
          },
          {
            ...defaultLineDataSet,
            data: []
          }
        ]
      },
      options: {
        ...defaultLineOptions
      },
      plugins: [
        {
          beforeDraw: beforeDrawForLines
        }
      ]
    };
    const configEngagement = {
      type: "line",
      data: {
        datasets: [
          {
            ...defaultLineDataSet,
            data: []
          }
        ]
      },
      options: {
        ...defaultLineOptions,
        scales: {
          ...defaultLineOptions.scales,
          yAxes: [
            {
              // display: false,
              ticks: {
                min: 0,
                max: 1,
                stepSize: 0.5,
                fontFamily:
                  "'Gill Sans MT-Regular', 'Gill Sans', 'Gill Sans MT', sans-serif",
                fontSize: 18,
                fontColor: "rgb(205,206,222)"
              },
              gridLines: {
                zeroLineColor: "rgba(0,0,0,0)",
                color: "rgba(0,0,0,0)"
              }
            }
          ]
        }
      },
      plugins: [
        {
          beforeDraw: beforeDrawForLines
        }
      ]
    };

    window.onload = function() {
      let ctxValence = document.getElementById("valence").getContext("2d");
      let ctxHappiness = document.getElementById("happiness").getContext("2d");
      let ctxEngagement = document.getElementById("attention").getContext("2d");
      window.happiness = new Chart(ctxHappiness, configHappiness);
      window.valence = new Chart(ctxValence, configValence);
      window.attention = new Chart(ctxEngagement, configEngagement);
    };
  }

  render() {
    const { realtimeData } = this.state;
    // console.log("state@render:", this.state.chartData);
    const chartXTicks = _.range(60, -10, -10);
    return (
      <div className="App">
        <div className="chart">
          <div className="header">
            <div className="title">EEG emotion visualisation</div>
            <div className="logoWrapper">
              <img src={logoSrc} alt="logo" className="logo" />
            </div>
          </div>
          <div key="blue blob" className="blueBlob" />
          <div key="emotion chart title" className="chartTitleWrapper">
            <span className="chartTitle">REALTIME EMOTION</span>
          </div>
          <div className="topChartWrapper">
            <div className="happinessWrapper">
              <canvas id="happiness" className="chartCanvas" />
            </div>
            <div key="details" className="detailsWrapper">
              {_.map(realtimeData, (value, key) => {
                return (
                  <div key={key} className="detailItemWrapper">
                    <div className="data">
                      <div className="dataValue">
                        {parseInt(value * 100) / 100}
                      </div>
                      <div className="dataname">
                        {_.startCase(_.toLower(key))}
                      </div>
                    </div>
                    <div className="explanation">
                        <div className="explanationTitle">{_.startCase(_.toLower(key))}</div>
                      <p className="explanationText">
                        {explanations[key]}
                      </p>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div key="valence chart title" className="chartTitleWrapper">
            <span className="chartTitle">VALENCE & </span>
            <span className="chartSecondTitle"> AROUSAL</span>
          </div>
          <div key="valence chart" className="singleChartWrapper">
            <canvas id="valence" className="chartCanvas" />
          </div>
          <div key="valence ticks" className="chartXTicks">
            {_.map(chartXTicks, value => {
              return value ? (
                <div className="chartXTickValue">{`T-${value}`}</div>
              ) : (
                <div className="chartXTickValue tickEndValue">T</div>
              );
            })}
          </div>
          <div key="attention chart title" className="chartTitleWrapper">
            <span className="chartTitle">ATTENTION</span>
          </div>
          <div key="attention chart" className="singleChartWrapper">
            <canvas id="attention" className="chartCanvas" />
          </div>
          <div key="attention ticks" className="chartXTicks">
            {_.map(chartXTicks, value => {
              return value ? (
                <div className="chartXTickValue">{`T-${value}`}</div>
              ) : (
                <div className="chartXTickValue tickEndValue">T</div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
