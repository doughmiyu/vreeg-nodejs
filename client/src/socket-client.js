import io from 'socket.io-client';
import events from './lib/socket-io-events'
export default class SocketClient {
    constructor() {
        this.socket = io('http://localhost:3001');
        this.setupSocket();
        this.chartData = {
            header : [],
            valence: [],
            happy: [],
            engagement: [],
            thinking: []
        }
    }

    setupSocket() {

        this.socket.on('testEvent', () => {
            console.log("received socket msg")
        });

        this.socket.on('disconnect', () => {
            this.socket.close();
        });

        this.socket.on('connect_error', () => {
            console.log('Waiting on server connection...')
        })

        this.socket.on(events.MESSAGE_RECEIVED, (data) => {
            console.log('data...',data)
            this.chartData.header.push(data.header)
            this.chartData.valence.push(data.valence)
            this.chartData.happy.push(data.happy)
            this.chartData.engagement.push(data.engagement)
            this.chartData.thinking.push(data.thinking)
        })
    }

    getChartData() {
        return this.chartData
    }
}