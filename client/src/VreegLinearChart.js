import React, { Component } from "react";
import _ from "lodash";
import { Line } from "react-chartjs-2";
import events from "./lib/socket-io-events";

const defaultLineDataSet = {
  fill: false,
  pointRadius: 0,
  borderWidth: 4,
  borderColor: "rgb(255,255,255)",
  lineTension: 0.3,
  cubicInterpolationMode: "monotone",
  data: []
};

const defaultLineScales = {
  xAxes: [
    {
      display: false,
      gridLines: {
        display: false
      },
      type: "realtime", // x axis will auto-scroll from right to left
      realtime: {
        // per-axis options
        duration: 60 * 1000, // data in the past 1 minute
        delay: 300,
        pause: false, // chart is not paused
        ttl: undefined,
        refresh: 100
      }
    }
  ],
  yAxes: [
    {
      display: false,
      gridLines: {
        display: false
      }
    }
  ]
};

const defaultLinePlugins = {
  streaming: {
    // per-chart option
    frameRate: 29 // chart is drawn 30 times every second
  }
};

const defaultLineOptions = {
  legend: {
    display: false
  },
  maintainAspectRatio: false,
  scales: {
    ...defaultLineScales
  },
  title: {
    display: false
  },
  plugins: {
    ...defaultLinePlugins
  }
};

class VreegLinearChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data:[]
    }
    this.refreshChartData = this.refreshChartData.bind(this);
    this.attachEventHandler = this.attachEventHandler.bind(this);
  }

  componentDidMount() {
    const { socket } = this.props;
    //attaching socket handler
    if (socket && typeof socket.on === "function")
      this.attachEventHandler(socket);
  }

  attachEventHandler(socket) {
    socket.on(events.MESSAGE_RECEIVED, this.refreshChartData);
  }

  componentWillReceiveProps(nextProps) {
    const { socket } = this.props;
    const { socket: nextSocket } = nextProps;
    if (
      _.isNil(socket) &&
      !_.isNil(nextSocket) &&
      _.isFunction(nextSocket.on)
    ) {
      this.attachEventHandler(nextSocket);
    }
  }

  componentWillUnmount() {
    const { socket } = this.props;
    socket.removeListener(events.MESSAGE_RECEIVED, this.refreshChartData);
  }

  refreshChartData(evtData) {
    const { type } = this.props;
    const {data} = this.state
    console.log(this.chart);
    this.setState({
        data: data.push({x: new Date().getTime(),
            y: evtData[type]})
    })

    // this.chart.update({
    //     preservation: true
    // })
  }
  render() {
    const {data} = this.state
    return (
      <Line
        ref={chart => (this.chart = chart)}
        data={{
          datasets: [{
              ...defaultLineDataSet,
              data
          }]
        }}
        options={{
          ...defaultLineOptions
        }}
      />
    );
  }
}

export default VreegLinearChart;
