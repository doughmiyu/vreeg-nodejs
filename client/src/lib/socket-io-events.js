export default {
    MESSAGE_RECEIVED: "socket.io_user_message_received",
    CLIENT_AUTHENTICATED: "socket.io_user_is_authenticated"
};
