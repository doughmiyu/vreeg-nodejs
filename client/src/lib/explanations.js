export default {
    valence: "the intrinsic attractiveness (positive valence) or averseness (negative valence) of an event, object, or situation.",
    arousal: "the state of being physiologically alert, awake, and attentive.",
    attention: "the behavioral and cognitive process of selectively concentrating on a discrete aspect of information while ignoring other perceivable information."
}
